# Lab 8 - Storage Familiarization on a Linux Machine

![](/Images/storage%20lab.png)

## Lab Goals/Summary

- In this lab we are going to cover two commands. The `df` and the `du` command. In previous labs we covered most of the commands to go over how we view storage. This lab will be short.
- In this lab I will be using Debian-based Linux OS (kali linux)

1. First we need to login to our Linux terminal.

![](/Images/kali%20terminal.png)

2. The `du` command will display the size of directories. We will run this command from the root of our system.

```
cd /
sudo du -h /etc
```

![](/Images/du%20command.png)

3. We can use the `df` command to check storage space on our drives.

`df -h /dev/nvme0n1p2`

`df -h /dev/`

![](/Images/df%20command.png)

# Lab Summary

- In this lab we covered the `du` and `df` command.