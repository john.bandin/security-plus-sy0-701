# Lab 10 Building a VPN

## Lab Goals/Summary

- Deploy a wireguard VPN Server on your raspberry PI and configure it as a split tunnel
- From this lab you will learn how to better secure your network environment
- Reference the chart of the bottom of this Lab to see the Private IP Address and Port Number you will use.


1. First we will go to our raspberry PI and set the IP address to static.

2. Next we will ensure that port forwarding is configured for our IP address on our Default Gateway.

3. Next, Open Terminal on your Raspberry Pi and run the command below, which will execute a script to install PiVPN (which has WireGuard built-in).

```
curl -L https://install.pivpn.io | bash
```
 ![](/Images/WG1.jpg)

 2. Wait for the process to install the necessary packages. When it’s done, you will be brought to a screen that will inform you that PiVPN will allow you to install OpenVPN or WireGuard on a Raspberry Pi. Select `OK`

 ![](/Images/WG2.jpg)

 3. At the next screen it will inform you to use a `static ip`. We will click `no` and then set our static IP again using this GUI. ***REFER TO THE CHART AT THE BOTTOM OF THIS LAB FOR YOUR STATIC IP SETTINGS***

 ![](/Images/WG3.jpg)

 4. You will now need to select a local user. Use the default `pi` user.

 ![](/Images/WG4.jpg)

 5. You will be asked to select a VPN type. Select `WireGuard.`

 ![](/Images/WG5.jpg)

 6. You **MAY** need to update the kernel on your raspberry, if you do select `OK` and then restart at step 3. 

 ![](/Images/WG6.jpg)

 7. WireGuard will now install. 

 ![](/Images/WG7.jpg)

 8. You will be asked to enter the port you’d like to use (default is 51820). But we will refer to our chart below for the correct port number to use. 

 ![](/Images/WG8.jpg)

 9. Select Yes. 

 ![](/Images/WG9.jpg)

 10. Now we will select our DNS servers. We will choose google's DNS.

 ![](/Images/WG10.jpg)

 11. The DNS servers that you select wil now be listed. Select `yes.`

 ![](/Images/WG12.jpg)

 13. You will now be prompted to use your public IP address or public DNS entry. We will use the public IP address.

 ![](/Images/WG13_new.jpg)

 14. Enable unattended-upgrades (unless you have a good reason not to) and proceed. The packages will now install.

 ![](/Images/WG16.jpg)

 15. The installation is now complete! Reboot your system.

 16. Now we will create the wg0 file so we can create our split-tunnel VPN. Run these commands to open the wireguard config file:

 ```
 sudo su
 nano /etc/wireguard/wg0.conf
 ```

 ```
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -D FORWARD -o wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
```

```
Change the "eth0" to your wifi ethernet adapter. use the `ip a` command to see the name of your wireless adapter.
```

![](/Images/WG25.jpg)

```
ensure to change the **listenport** number to the correct port number on the chart below.
```

18. Now we will create our VPN profiles for our users, using the command below.

```
sudo pivpn add
```

![](/Images/Screenshot%202023-09-11%20at%2010.03.28%20PM.png)

19. Now, since we are building a split tunnel VPN we will edit our VPN profile to only allow our LAN IPs.

```
nano PiVPN-ST.conf
```

![](/Images/WG20.jpg)

![](/Images/WG21.jpg)

20. Now we will generate a QR code, so our mobile phones can download our VPN profile straight to the `wireguard` application. Run the following command.

```
pivpn -qr [PROFILE_NAME]
```

![](/Images/WG22.jpg)

21. A QR code will be generated. Scan this code with your phone, import the profile and you’re done!

| Student Name | IP Address | Port Number |
| ----------- | ----------- | ----------- |
| Student01 | 192.168.0.70 | 51821 |
| Student02 | 192.168.0.71 | 51822 |
| Student03 | 192.168.0.72 | 51823 |
| Student04 | 192.168.0.73 | 51824 |
| Student05 | 192.168.0.74 | 51825 |
| Student06 | 192.168.0.75 | 51826 |
| Student07 | 192.168.0.76 | 51827 |
| Student08 | 192.168.0.77 | 51828 |
| Student09 | 192.168.0.78 | 51829 |
| Student10 | 192.168.0.79 | 51920 |
| Student11 | 192.168.0.80 | 51921 |
| Student12 | 192.168.0.81 | 51922 |
| Student13 | 192.168.0.82 | 51923 |
| Student14 | 192.168.0.83 | 51924 |
| Student15 | 192.168.0.84 | 51925 |
| Student16 | 192.168.0.85 | 51926 |
| Student17 | 192.168.0.86 | 51927 |
| Student18 | 192.168.0.87 | 51928 |
| Student19 | 192.168.0.88 | 51929 |
| Student20 | 192.168.0.89 | 51300 |
| Student21 | 192.168.0.90 | 51301 |
| Student22 | 192.168.0.91 | 51302 |
| Student23 | 192.168.0.92 | 51303 |
| Student24 | 192.168.0.93 | 51304 |
| Student25 | 192.168.0.94 | 51305 |
| Student26 | 192.168.0.95 | 51306 |
| Student27 | 192.168.0.96 | 51307 |
| Student28 | 192.168.0.97 | 51308 |
| Student29 | 192.168.0.98 | 51309 |
| Student30 | 192.168.0.99 | 51310 |
| Student31 | 192.168.0.101 | 51311 |
| Student32 | 192.168.0.102 | 51312 |
| Student33 | 192.168.0.103 | 51313 |
| Student34 | 192.168.0.104 | 51314 |
| Student35 | 192.168.0.105 | 51315 |
| Student36 | 192.168.0.106 | 51316 |
| Student37 | 192.168.0.107 | 51317 |
| Student38 | 192.168.0.108 | 51318 |
| Student39 | 192.168.0.109 | 51319 |
| Student40 | 192.168.0.110 | 51320 |


